package com.example.sneakersworld.model

data class Sneaker (
    val id: String,
    var foto: String,
    var nombre: String,
    var marca: String,
    var descripcion: String,
    var talla: List<Int>,
    var precio: Double
)

