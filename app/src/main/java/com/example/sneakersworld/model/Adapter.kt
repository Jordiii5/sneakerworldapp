package com.example.sneakersworld.model

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.sneakersworld.R
import com.example.sneakersworld.databinding.ItemSneakerBinding

class Adapter(private val sneakers: List<Sneaker>, private val listener: com.example.sneakersworld.ViewModel.OnClickListener): RecyclerView.Adapter<Adapter.ViewHolder>() {
    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemSneakerBinding.bind(view)
        fun setListener(sneaker: Sneaker){
            binding.root.setOnClickListener{
                listener.onClick(sneaker)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_sneaker, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sneakers.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sneaker = sneakers[position]

        with(holder){
            setListener(sneaker)
            binding.sneakerName.text = sneaker.nombre
            binding.sneakerPrice.text = sneaker.precio.toString()
            Glide.with(context)
                .load(sneaker.foto)
                .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(25)))
//                .transform(RoundedCorners(30))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.sneakerImage)

        }
    }

}
