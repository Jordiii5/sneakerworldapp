package com.example.sneakersworld.model


data class User(val username: String, val password: String)
