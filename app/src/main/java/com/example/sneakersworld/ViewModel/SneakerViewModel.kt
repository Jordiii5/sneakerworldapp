package com.example.sneakersworld.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sneakersworld.model.Sneaker
import com.example.sneakersworld.model.User
import com.example.sneakersworld.view.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SneakerViewModel: ViewModel() {
    var repository= Repository("","")
    var data = MutableLiveData<List<Sneaker>>()
    var sneakerDetall = MutableLiveData<Sneaker>()
    var currentUser = MutableLiveData<User>()

    init {
        fetchData()
    }

    fun fetchData(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getSneaker("sneakers")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    data.postValue(response.body() as List<Sneaker>)
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
}