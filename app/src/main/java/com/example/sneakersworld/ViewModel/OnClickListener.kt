package com.example.sneakersworld.ViewModel

import com.example.sneakersworld.model.Sneaker

interface OnClickListener {
    fun onClick(sneaker: Sneaker)
}