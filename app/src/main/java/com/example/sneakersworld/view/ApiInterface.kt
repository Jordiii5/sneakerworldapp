package com.example.sneakersworld.view



import com.burgstaller.okhttp.digest.DigestAuthenticator
import com.example.sneakersworld.model.Sneaker
import com.example.sneakersworld.model.User
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {

    @GET()
    suspend fun getSneakers(@Url url :String): Response<List<Sneaker>>

    @POST("login")
    suspend fun login(@Body user: User): Response<ResponseBody>

    @POST("register")
    suspend fun register(@Body user: User): Response<ResponseBody>

    companion object {
        val BASE_URL = "http://192.168.1.151:8080/"
        //ip jordi 192.168.56.1
        //ip ordenador gerard clase 172.23.5.128
        //ip gerard 192.168.1.151


        fun create(username: String, password: String): ApiInterface {
            val digestAuthenticator = DigestAuthenticator(com.burgstaller.okhttp.digest.Credentials(username, password))

            val client = OkHttpClient.Builder()
                .authenticator(digestAuthenticator)
                .build()


            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

}