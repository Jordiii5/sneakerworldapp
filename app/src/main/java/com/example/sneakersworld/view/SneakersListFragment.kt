package com.example.sneakersworld.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sneakersworld.R
import com.example.sneakersworld.ViewModel.OnClickListener
import com.example.sneakersworld.ViewModel.SneakerViewModel
import com.example.sneakersworld.databinding.FragmentSneakersListBinding
import com.example.sneakersworld.model.Adapter
import com.example.sneakersworld.model.Sneaker

class SneakersListFragment : Fragment(), OnClickListener {
   lateinit var binding: FragmentSneakersListBinding
   private lateinit var userAdapter: Adapter
   private lateinit var linearLayoutManager: RecyclerView.LayoutManager
   private val viewModel: SneakerViewModel by activityViewModels()


   override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
   ): View? {
      binding = FragmentSneakersListBinding.inflate(layoutInflater)
      val mainActivity = requireActivity() as MainActivity
      mainActivity.setBottomNavigationVisible(true)
      return binding.root
   }

   override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
      super.onViewCreated(view, savedInstanceState)

      val sneakerViewModel = ViewModelProvider(requireActivity())[SneakerViewModel::class.java]
      sneakerViewModel.data.observe(viewLifecycleOwner){
         setUpRecyclerView(it)
      }

   }

   private fun setUpRecyclerView(lista: List<Sneaker>){
      userAdapter = Adapter(lista, this)
      linearLayoutManager = LinearLayoutManager(context)
      binding.recyclerView.apply {
         setHasFixedSize(true)
         layoutManager = linearLayoutManager
         adapter = userAdapter
      }
   }

   private fun getSneakers(): MutableList<Sneaker>{
      val sneakers = mutableListOf<Sneaker>()
      sneakers.add(Sneaker("id", "url_imatge", "nombre", "marca", "descripcion", listOf(1), 0.0))
      return sneakers
   }

   override fun onClick(sneaker: Sneaker) {
      val sneakerViewModel = ViewModelProvider(requireActivity()) [SneakerViewModel::class.java]
      sneakerViewModel.sneakerDetall.postValue(sneaker)
      findNavController().navigate(R.id.action_sneakersListFragment_to_detallFragment)
   }
}