package com.example.sneakersworld.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sneakersworld.databinding.FragmentCestaBinding



class CestaFragment : Fragment() {
    lateinit var binding: FragmentCestaBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCestaBinding.inflate(layoutInflater)
        return binding.root
    }
}

