package com.example.sneakersworld.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.sneakersworld.R
import com.example.sneakersworld.ViewModel.SneakerViewModel
import com.example.sneakersworld.databinding.FragmentLoginBinding
import com.example.sneakersworld.model.User
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class LoginFragment : Fragment() {
    lateinit var binding: FragmentLoginBinding
    private val viewModel: SneakerViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentLoginBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)

        binding.loginButton.setOnClickListener {
            if (binding.usernameEdittext.text.toString()!="" && binding.passwordEdittext.text.toString() != ""){
                viewModel.currentUser.value = User(binding.usernameEdittext.text.toString(),binding.passwordEdittext.text.toString())
                viewModel.repository = Repository(binding.usernameEdittext.text.toString(),binding.passwordEdittext.text.toString())
                CoroutineScope(Dispatchers.IO).launch {
                    val repository = Repository(binding.usernameEdittext.text.toString(),binding.passwordEdittext.text.toString())
                    val response = repository.login(viewModel.currentUser.value!!)
                    withContext(Dispatchers.Main) {
                        if(response.isSuccessful){
                            Toast.makeText(context, "Welcome ${viewModel.currentUser.value!!.username}", Toast.LENGTH_SHORT).show()
                            findNavController().navigate(R.id.action_loginFragment_to_sneakersListFragment)
                        }
                        else{
                            Toast.makeText(context, "Error to login", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

            }
        }
        binding.signup.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }
}