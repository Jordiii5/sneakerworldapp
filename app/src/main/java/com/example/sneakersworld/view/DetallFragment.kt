package com.example.sneakersworld.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.sneakersworld.R
import com.example.sneakersworld.ViewModel.SneakerViewModel
import com.example.sneakersworld.databinding.FragmentDetallBinding
import com.example.sneakersworld.model.Sneaker


class  DetallFragment : Fragment() {
    lateinit var binding: FragmentDetallBinding
    lateinit var sneakersDetall : Sneaker

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetallBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sneakerViewModel = ViewModelProvider(requireActivity())[SneakerViewModel::class.java]

        sneakerViewModel.sneakerDetall.observe(viewLifecycleOwner){
            sneakersDetall = it
            Glide.with(requireContext())
                .load(sneakersDetall.foto)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.imageView)
            binding.nombre.text = sneakersDetall.nombre
            binding.marcatext.text = sneakersDetall.marca
            binding.descripciontext.text = sneakersDetall.descripcion
            binding.preciotext.text = sneakersDetall.precio.toString()
        }

        binding.botonatras.setOnClickListener {
            findNavController().navigate(R.id.action_detallFragment_to_sneakersListFragment)
        }

    }

}



