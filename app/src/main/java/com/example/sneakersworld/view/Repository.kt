package com.example.sneakersworld.view

import com.example.sneakersworld.model.User


class Repository(username: String, password: String) {

    val apiInterface = ApiInterface.create(username,password)

    suspend fun login(user: User) = apiInterface.login(user)
    suspend fun register(user: User) = apiInterface.register(user)
    suspend fun getSneaker(url : String) = apiInterface.getSneakers(url)


    //suspend fun getSneakerbyMarca() = apiInterface.getSneakerbyMarca()
}